# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


## Report

### Finished goal item

1. A complete game process
   * Menu
   <br><img src="https://i.imgur.com/kwjfKom.png" width="634px" height="436px"></img>
   * Playing
   <br><img src="https://i.imgur.com/RVipDtc.png" width="634px" height="436px"></img>
   * Gameover
   <br><img src="https://i.imgur.com/Jl5YIHu.png" width="634px" height="436px"></img>
2. Follow the basic rules of "小朋友下樓梯"
   * Injured when touching nails
   * Healed when standing on the platform
   * 12 max HP, minus 5 each time injured
   <br><img src="https://i.imgur.com/cegLkXU.png" width="634px" height="436px"></img>
   * Player die when HP down to 0 or out-of-world
   * Every few seconds increase the 'step' by 1
   <br><img src="https://i.imgur.com/7e2wfLM.png" width="634px" height="436px"></img>
3. Correct physical properties and behaviors
   * Collision between players, platforms and walls
   * Gravity
4. Some interesting traps or special mechanisms
   * All 4 special platform that "小朋友下樓梯" has :
     * Nails - make players injured
     * Conveyor - move players horizontally
     * Trampoline - make players jump into the air
     * Fake - can only stand for a while
   * Nails on the ceiling
     * The effect is the same as the nails platform
5. Additional sound effects and UI to enrich your game
   * Background music
   * Different sound at different platform
   * Sound effects of players
   * Beautiful UIs
6. Score storage and leaderboard
   * Player can store name and score when the game is over
   * Top 5 leaderboard at start menu
7. Appearance
   * Almost the same as "小朋友下樓梯"
8. Other creative features
   * Dual players mode supported
   <br><img src="https://i.imgur.com/wtvvgRl.png" width="634px" height="436px"></img>
   * Emitter, camera flash effect and tweens are used
   * Pause and return-to-menu button in the playing state
   <br><img src="https://i.imgur.com/qQJXXoH.png" width="634px" height="436px"></img>
   <br><img src="https://i.imgur.com/GVv7lki.png" width="634px" height="436px"></img>
   * Every 10 steps will increase the numbers of the nails platform and increase the speed of decline
   