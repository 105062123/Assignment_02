var loadState = {
    preload: function () {
        // // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.4, 0.5);
        // // Display the progress bar
        var progressBar = game.add.sprite(game.width/2 - 48, 200, 'healthbar');
        game.load.setPreloadSprite(progressBar);

        // Load all game assets
        game.load.image('backgroundBig', 'assets/backgroundBig.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.image('score', 'assets/score.png');
        game.load.image('score2', 'assets/score2.png');
        game.load.image('background', 'assets/background.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('gameover', 'assets/gameover.png');
        game.load.image('pause', 'assets/pause.png');
        
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('player2', 'assets/player2.png', 32, 32);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        
        game.load.bitmapFont('font', 'assets/font_0.png', 'assets/font.fnt');

        game.load.audio('backgroundMusic', 'assets/backgroundMusic.wav');
        game.load.audio('normalSound', 'assets/normalSound.wav');
        game.load.audio('trampolineSound', 'assets/trampolineSound.wav');
        game.load.audio('conveyorSound', 'assets/conveyorSound.wav');
        game.load.audio('injuredSound', 'assets/injuredSound.wav');
        game.load.audio('fakeSound', 'assets/fakeSound.wav');
        game.load.audio('dieSound', 'assets/dieSound.wav');
        game.load.audio('end', 'assets/end.wav');

        },

    create: function() {
        // Go to the menu state
        game.state.start('menu');
    }
}; 