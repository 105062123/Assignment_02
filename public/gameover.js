var gameOverState = {
    preload: function () {},
    create: function() {
        
        var player = prompt("Please enter your name", "name");

        if(player != null){
            var database = firebase.database().ref('/scoreboard');
            database.push().set({
                score: game.global.scoreValue,
                nscore: -game.global.scoreValue,
                name: player
            });
        }

        this.background = game.add.tileSprite(40, 64, 384, 354, 'background');
        this.leftWall = game.add.tileSprite(24, 64, 16, 354, 'wall');
        this.rightWall = game.add.tileSprite(424, 64, 16, 354, 'wall');
        this.backgroundBig = game.add.image(0, 0, 'backgroundBig');

        var gameover = game.add.sprite(232, 200, 'gameover');
        gameover.anchor.setTo(0.5, 0.5);
        game.add.tween(gameover.scale).to({x: 3, y: 3}, 5000).start();

        var reStart_label = game.add.text(232, 350, 'Play again!', { font: '24px Arial', fill: '#fff' });
        reStart_label.anchor.setTo(0.5, 0.5);
        reStart_label.inputEnabled = true;
        reStart_label.events.onInputUp.add(this.reStart);
    
        this.end = game.add.audio('end');
        this.end.loop = false; 
        this.end.play();

    },
    
    reStart: function() {
        gameOverState.end.stop();
        game.state.start('menu');
    },
}; 