var playState = {
    preload: function () {},
    create: function() {
        this.platformSpeed = 80;

        game.world.setBounds(0, 0, 634, 417);

        this.background = game.add.tileSprite(40, 64, 384, 354, 'background');
        this.background.fixedToCamera = true;

        this.leftWall = game.add.tileSprite(24, 64, 16, 354, 'wall');
        this.leftWall.fixedToCamera = true;
        game.physics.arcade.enable(this.leftWall);
        this.leftWall.body.immovable = true;

        this.rightWall = game.add.tileSprite(424, 64, 16, 354, 'wall');
        this.leftWall.fixedToCamera = true;
        game.physics.arcade.enable(this.rightWall);
        this.rightWall.body.immovable = true;

        
        this.platforms = game.add.group();
        this.platforms.enableBody = true;
        this.platforms.createMultiple(20, 'normal');
        this.platforms.createMultiple(8, 'nails');
        this.platforms.createMultiple(2, 'conveyorRight');
        this.platforms.createMultiple(2, 'conveyorLeft');
        this.platforms.createMultiple(2, 'trampoline');
        this.platforms.createMultiple(2, 'fake');
        this.platforms.setAll('body.immovable', true);
        this.platforms.setAll('body.checkCollision.left', false);
        this.platforms.setAll('body.checkCollision.right', false);
        this.platforms.setAll('body.checkCollision.down', false);

        for(var i=0; i<9; i++){
            var platform;
            if(i == 7){
                platform = this.platforms.getFirstDead();
                platform.reset(184, 48*i+64, 'normal', 0);
            }else{
                do {platform = this.platforms.getRandom();} while (platform.alive)
                var newY, key = platform.key;
                if(key == 'normal'){
                    newY = 0;
                }else if(key == 'nails'){
                    newY = 17;
                }else if(key == 'conveyorRight' || key == 'conveyorLeft'){
                    newY = 0;
                    platform.animations.add('test', [0, 1, 2, 3], 20, true);
                    platform.animations.play('test');
                }else if(key == 'trampoline'){
                    platform.animations.add('test', [4, 5, 0, 1, 2, 3], 18, false);
                    newY = 6;
                    platform.frame = 3;
                }else if(key == 'fake'){
                    platform.animations.add('test', [0, 1, 2, 3, 4, 5, 0], 6, false);
                    newY = 9;
                }
                platform.reset(40 + Math.floor(Math.random()*19)*(384/24), 48*i-newY+64, 'normal', 0);
                platform.body.setSize(97, 16, 0, newY);
            }
            platform.body.velocity.y = -this.platformSpeed;
            platform.checkWorldBounds = true; 
            platform.body.checkCollision.left = false;
            platform.body.checkCollision.right = false;
            platform.body.checkCollision.down = false;
            platform.outOfBoundsKill = true;
        }
        
        game.time.events.loop(700, this.addPlatform, this);
        game.time.events.loop(27000, this.addNails, this);

        this.ceiling = game.add.sprite(40, 64, 'ceiling');
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.immovable = true;
        this.ceiling.body.setSize(400, 5);

        this.backgroundBig = game.add.image(0, 0, 'backgroundBig');

        if(!game.global.player2Enable){
            this.player = game.add.sprite(216, 367, 'player');
            this.scorePic = game.add.sprite(48, 16, 'score');
            this.healthbar = game.add.sprite(48, 32,'healthbar');
            this.scoreText = game.add.bitmapText(294, 16, 'font', '001', 30);
        }else{
            this.player = game.add.sprite(240, 367, 'player');
            this.scorePic = game.add.sprite(24, 16, 'score2');
            this.healthbar = game.add.sprite(344, 32,'healthbar');
            this.scoreText = game.add.bitmapText(203, 16, 'font', '001', 30);
            
            this.player2 = game.add.sprite(192, 367, 'player2');
            game.physics.arcade.enable(this.player2);
            this.player2.body.setSize(26, 32, 3, 0);
            this.player2.animations.add('left', [0, 1, 2, 3], 24);
            this.player2.animations.add('left_injured', [4, 5, 6, 7], 24);
            this.player2.animations.add('right', [9, 10, 11, 12], 24);
            this.player2.animations.add('right_injured', [13, 14, 15, 16], 24);
            this.player2.animations.add('left_fly', [18, 19, 20, 21], 24);
            this.player2.animations.add('left_fly_injured', [22, 23, 24, 25], 24);
            this.player2.animations.add('right_fly', [27, 28, 29, 30], 24);
            this.player2.animations.add('right_fly_injured', [31, 32, 33, 34], 24);
            this.player2.animations.add('fly', [36, 37, 38, 39], 24);
            this.player2.animations.add('fly_injured', [40, 41, 42, 43], 24);
            this.player2.animations.add('idle', [8], 1, false);
            this.player2.animations.add('idle_injured', [8, 17], 24, true);
            this.player2.frame = 8;
            this.player2.body.gravity.y = 1000;
            this.player2.bodySpeed = 0;
            this.player2.groundSpeed = 0;

            this.zKey = game.input.keyboard.addKey(Phaser.KeyCode.Z);
            this.xKey = game.input.keyboard.addKey(Phaser.KeyCode.X);
            
            this.player2.health = 12;
            this.healthCrop2 = new Phaser.Rectangle(0, 0, 96, 16);
            
            this.healthbar2 = game.add.sprite(24, 32,'healthbar');
            this.healthbar2.cropEnabled = true;
            this.healthbar2.crop(this.healthCrop2);
            
            this.player2.injured = 0;
            this.player2.onTheGround = true;
        }
        game.physics.arcade.enable(this.player);
        this.player.body.setSize(26, 32, 3, 0);
        this.player.animations.add('left', [0, 1, 2, 3], 24);
        this.player.animations.add('left_injured', [4, 5, 6, 7], 24);
        this.player.animations.add('right', [9, 10, 11, 12], 24);
        this.player.animations.add('right_injured', [13, 14, 15, 16], 24);
        this.player.animations.add('left_fly', [18, 19, 20, 21], 24);
        this.player.animations.add('left_fly_injured', [22, 23, 24, 25], 24);
        this.player.animations.add('right_fly', [27, 28, 29, 30], 24);
        this.player.animations.add('right_fly_injured', [31, 32, 33, 34], 24);
        this.player.animations.add('fly', [36, 37, 38, 39], 24);
        this.player.animations.add('fly_injured', [40, 41, 42, 43], 24);
        this.player.animations.add('idle', [8], 1, false);
        this.player.animations.add('idle_injured', [8, 17], 24, true);
        this.player.frame = 8;
        this.player.body.gravity.y = 1000;
        this.player.bodySpeed = 0;
        this.player.groundSpeed = 0;

        this.player.health = 12;
        this.healthMax = 12;

        this.healthCrop = new Phaser.Rectangle(0, 0, 96, 16);

        this.healthbar.cropEnabled = true;
        this.healthbar.crop(this.healthCrop);

        this.scoreText.angle = 180;
        game.global.scoreValue = 1;

        game.time.events.loop(3000, this.increaseScore, this);

        this.backgroundMusic = game.add.audio('backgroundMusic');
        this.backgroundMusic.loop = true; 
        this.backgroundMusic.play();
        
        this.normalSound = game.add.audio('normalSound');
        this.trampolineSound = game.add.audio('trampolineSound');
        this.conveyorSound = game.add.audio('conveyorSound');
        this.injuredSound = game.add.audio('injuredSound');
        this.fakeSound = game.add.audio('fakeSound');
        this.dieSound = game.add.audio('dieSound');


        this.emitter = game.add.emitter(0, 0, 25);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(5, 0, 5, 0, 800);
        this.emitter.gravity = 0;
        
        this.cursor = game.input.keyboard.createCursorKeys();
        this.player.injured = 0;
        this.player.onTheGround = true;

        pause_label = game.add.text(512, 305, '    ', { font: '40px Arial', fill: '#fff' });
        pause_label.inputEnabled = true;
        pause_label.events.onInputUp.add(function () {
            playState.backgroundMusic.pause();
            game.paused = true;

            this.pause_sign = game.add.sprite(232, 240, 'pause');
            this.pause_sign.anchor.setTo(0.5, 0.5);
        });
        game.input.onDown.add(this.unpause, self);

        menu_label = game.add.text(512, 364, '    ', { font: '40px Arial', fill: '#fff' });
        menu_label.inputEnabled = true;
        menu_label.events.onInputUp.add(function () {
            playState.backgroundMusic.stop();
            game.state.start('menu');
        });
    },

    update: function() {
        this.background.tilePosition.y -= this.platformSpeed * 0.43 / 60;
        this.leftWall.tilePosition.y -= this.platformSpeed / 60;
        this.rightWall.tilePosition.y -= this.platformSpeed / 60;

        
        if(this.player.injured > 0)this.player.injured--;
        this.healthCrop.width = 96 * this.player.health / this.healthMax;
        this.healthbar.updateCrop();

        this.player.body.velocity.x = this.player.bodySpeed + this.player.groundSpeed;
        
        if(game.global.player2Enable){
            this.player2.body.velocity.x = this.player2.bodySpeed + this.player2.groundSpeed;
            
            game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
            game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
            game.physics.arcade.collide(this.player, this.ceiling, this.touchCeiling, null, this);
            game.physics.arcade.collide(this.player2, [this.leftWall, this.rightWall]);
            game.physics.arcade.collide(this.player2, this.platforms, this.effect, null, this);
            game.physics.arcade.collide(this.player2, this.ceiling, this.touchCeiling, null, this);
            this.movePlayer();
            this.movePlayer2();
            game.physics.arcade.collide(this.player2, this.player, this.playerCollision, null, this);
            
            game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
            game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
            game.physics.arcade.collide(this.player, this.ceiling, this.touchCeiling, null, this);
            game.physics.arcade.collide(this.player2, [this.leftWall, this.rightWall]);
            game.physics.arcade.collide(this.player2, this.platforms, this.effect, null, this);
            game.physics.arcade.collide(this.player2, this.ceiling, this.touchCeiling, null, this);
            game.physics.arcade.collide(this.player2, this.player, this.playerCollision, null, this);
            
            if(this.player2.injured > 0)this.player2.injured--;
            this.healthCrop2.width = 96 * this.player2.health / this.healthMax;
            this.healthbar2.updateCrop();
            if ((!this.player2.inWorld || this.player2.health == 0) && this.player2.alive) { this.playerDie(this.player2);}
        }else{
            game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
            game.physics.arcade.collide(this.player, this.platforms, this.effect, null, this);
            game.physics.arcade.collide(this.player, this.ceiling, this.touchCeiling, null, this);
            this.movePlayer();
        }

        console.log(this.player.body.velocity.x);
        
        
        if ((!this.player.inWorld || this.player.health == 0) && this.player.alive) { this.playerDie(this.player);}
    },

    playerCollision: function(player, player2) {
        if(player.body.y == player2.body.y)return;
        if(player.body.y > player2.body.y){
            if(player.body.velocity.y < -this.platformSpeed && player2.body.velocity.y > -400){
                player2.body.velocity.y += player.body.velocity.y;
                player.body.velocity.y = 0;
            }
        }else{
            if(player2.body.velocity.y < -this.platformSpeed && player.body.velocity.y > -400){
                player.body.velocity.y += player2.body.velocity.y;
                player2.body.velocity.y = 0;
            }
        }
    },

    movePlayer: function() {
        var injureCheck = '';
        if(this.player.injured > 0) injureCheck = '_injured';
        if(!this.player.body.touching.down){
            this.player.onTheGround = false;
            this.player.groundSpeed = 0;
        }

        if (this.cursor.left.isDown){
            this.player.bodySpeed = -180;
            if(this.player.body.onFloor() || this.player.body.touching.down)
                this.player.animations.play('left' + injureCheck);
            else
                this.player.animations.play('left_fly' + injureCheck);
        }else if(this.cursor.right.isDown){
            this.player.bodySpeed = 180;
            if(this.player.body.onFloor() || this.player.body.touching.down)
                this.player.animations.play('right' + injureCheck);
            else
                this.player.animations.play('right_fly' + injureCheck);
        }else{
            if(this.player.body.onFloor() || this.player.body.touching.down){
                this.player.animations.play('idle' + injureCheck);
            }else{
                this.player.animations.play('fly' + injureCheck);
            }
            this.player.bodySpeed = 0;
        }
    },

    movePlayer2: function() {
        var injureCheck = '';
        if(this.player2.injured > 0) injureCheck = '_injured';
        if(!this.player2.body.touching.down){
            this.player2.onTheGround = false;
            this.player2.groundSpeed = 0;
        }

        if (this.zKey.isDown){
            this.player2.bodySpeed = -180;
            if(this.player2.body.onFloor() || this.player2.body.touching.down)
                this.player2.animations.play('left' + injureCheck);
            else
                this.player2.animations.play('left_fly' + injureCheck);
        }else if(this.xKey.isDown){
            this.player2.bodySpeed = 180;
            if(this.player2.body.onFloor() || this.player2.body.touching.down)
                this.player2.animations.play('right' + injureCheck);
            else
                this.player2.animations.play('right_fly' + injureCheck);
        }else{
            if(this.player2.body.onFloor() || this.player2.body.touching.down){
                this.player2.animations.play('idle' + injureCheck);
            }else{
                this.player2.animations.play('fly' + injureCheck);
            }
            this.player2.bodySpeed = 0;
        }
    },

    addPlatform: function() {
        do {var platform = this.platforms.getRandom();} while (platform.alive)
        var newY, key = platform.key;
        if(key == 'normal'){
            newY = 0;
        }else if(key == 'nails'){
            newY = 17;
        }else if(key == 'conveyorRight' || key == 'conveyorLeft'){
            newY = 0;
            platform.animations.add('test', [0, 1, 2, 3], 20, true);
            platform.animations.play('test');
        }else if(key == 'trampoline'){
            platform.animations.add('test', [4, 5, 0, 1, 2, 3], 18, false);
            newY = 6;
            platform.frame = 3;
        }else if(key == 'fake'){
            platform.animations.add('test', [0, 1, 2, 3, 4, 5, 0], 6, false);
            newY = 9;
        }
        platform.reset(40 + Math.floor(Math.random()*19)*(384/24), 416-newY);
        platform.body.velocity.y = -this.platformSpeed;
        platform.checkWorldBounds = true; 
        platform.outOfBoundsKill = true;
        platform.body.checkCollision.left = false;
        platform.body.checkCollision.right = false;
        platform.body.checkCollision.down = false;
        platform.body.setSize(97, 16, 0, newY);
    },

    addNails: function() {
        this.platformSpeed += 3;
        this.platforms.createMultiple(5, 'nails');
        this.platforms.setAll('body.immovable', true);
        this.platforms.setAll('body.checkCollision.left', false);
        this.platforms.setAll('body.checkCollision.right', false);
        this.platforms.setAll('body.checkCollision.down', false);
        this.platforms.setAll('body.velocity.y', -this.platformSpeed);
    },

    effect: function(player, platform) {
        if(!player.body.touching.down || player.body.y > platform.body.y - 32 || player.onTheGround == true)return;
        player.onTheGround = true;
        var key = platform.key;
        if(key == 'nails'){
            game.camera.flash(0xff0000, 50);

            this.injuredSound.play();
            player.health -= 5;
            if(player.health < 0) player.health = 0;

            player.animations.play('idle_injured');
            player.injured = 50;
        }else{
            if(player.health < 12)
                player.health++;
            if(key == 'normal') this.normalSound.play();
            else if(key == 'conveyorRight' || key == 'conveyorLeft') this.conveyorSound.play();
            else if(key == 'trampoline') this.trampolineSound.play();
            else if(key == 'fake') this.fakeSound.play();
        }
        
        if(key == 'conveyorRight'){
            if(player.body.touching.down)
                // player.x += 1.4;
                player.groundSpeed = 84;
        }else if(key == 'conveyorLeft'){
            if(player.body.touching.down)
                // player.x -= 1.4;
                player.groundSpeed = -84;
        }else if(key == 'trampoline'){
            platform.animations.stop('test');
            platform.animations.play('test');
            player.body.velocity.y = -365;
        }else if(key == 'fake'){
            setTimeout(function() {
                platform.body.checkCollision.up = false;
                platform.animations.play('test');
                setTimeout(function() {
                    platform.body.checkCollision.up = true;
                }, 1000);
            }, 100);
        }
    },

    touchCeiling: function(player, ceiling) {
        if(player.injured < 45){
            this.injuredSound.play();
            game.camera.flash(0xff0000, 50);
            player.health -= 5;
            if(player.health < 0) player.health = 0;
            player.injured = 50;
        }
        this.player.y += 1;
        if(game.global.player2Enable)
        this.player2.y += 1;
    },

    increaseScore: function() {
        if(!game.global.player2Enable)
            if(!this.player.alive)return;
        else
            if(!this.player.alive && !this.player2.alive)return;
        game.global.scoreValue++;
        var newScoreText = '' + game.global.scoreValue;
        this.scoreText.text = newScoreText.padStart(3, "0");
    },

    unpause: function() {
        if(game.paused){
            game.paused = false;
            playState.backgroundMusic.resume();
            this.pause_sign.destroy();
        }
    },

    playerDie: function(player) {
        player.kill();
        
        this.emitter.x = player.x + 16;
        this.emitter.y = player.y + 10;
        this.emitter.start(true, 1800, null, 15);
        
        if(!player.inWorld)
            this.dieSound.play();
        
        if(!game.global.player2Enable){
            this.backgroundMusic.stop();
            game.time.events.add(1800,function() {
                game.state.start('gameover');
            },this);
        }else{
            if(!this.player.alive && !this.player2.alive){
                this.backgroundMusic.stop();
                game.time.events.add(1800,function() {
                    game.state.start('gameover');
                },this);
            }else{
                if(this.player.alive)
                    game.add.sprite(24, 32,'gameover');
                else
                    game.add.sprite(344, 32,'gameover');
            }
        }
    }
}